package week04

object exprs {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(150); 

	def show(e: Expr): String = e match {
		case Number(n) => n.toString
		case Sum(l, r) => show(l) + " + " + show(r)
	};System.out.println("""show: (e: week04.Expr)String""");$skip(35); val res$0 = 
	
	show(Sum(Number(3), Number(2)));System.out.println("""res0: String = """ + $show(res$0))}
	
	
}