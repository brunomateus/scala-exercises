object list {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(185); 
  def flatten(xs: List[Any]): List[Any] = xs match {
  	case List() => List()
  	case (y:List[Any]) :: ys => flatten(y ::: ys)
  	case y :: ys =>  y :: flatten(ys)
  	
  };System.out.println("""flatten: (xs: List[Any])List[Any]""");$skip(23); val res$0 = 
  
 flatten(List(1,1));System.out.println("""res0: List[Any] = """ + $show(res$0));$skip(28); val res$1 = 
 
 flatten(List(List(1,1)));System.out.println("""res1: List[Any] = """ + $show(res$1));$skip(31); val res$2 = 
 
 flatten(List(List(1,1), 2));System.out.println("""res2: List[Any] = """ + $show(res$2));$skip(37); val res$3 = 
 
 flatten(List(List(List(1,1), 2)));System.out.println("""res3: List[Any] = """ + $show(res$3));$skip(52); val res$4 = 

 flatten(List(List(1, 1), 2, List(3, List(5, 8))));System.out.println("""res4: List[Any] = """ + $show(res$4))}
}