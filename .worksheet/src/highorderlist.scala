object highorderlist {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(149); 
  
	def squareList(xs: List[Int]): List[Int] = xs match {
    case Nil     => xs
    case y :: ys => y*y :: squareList(ys)
  };System.out.println("""squareList: (xs: List[Int])List[Int]""");$skip(69); 

  def squareList2(xs: List[Int]): List[Int] =
    xs map (n => n*n);System.out.println("""squareList2: (xs: List[Int])List[Int]""");$skip(179); 

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil      => Nil
    case x :: xs1 =>
    	val (first, last) = xs span((n => x == n))
    	first :: pack(last)
  };System.out.println("""pack: [T](xs: List[T])List[List[T]]""");$skip(92); 
  
  def encode[T](xs: List[T]): List[(T, Int)] =
  	pack(xs) map (x => (x.head, x.length));System.out.println("""encode: [T](xs: List[T])List[(T, Int)]""");$skip(41); 
  	
   
      
  val l = List(1,2,3,4,5);System.out.println("""l  : List[Int] = """ + $show(l ));$skip(19); val res$0 = 
  
  squareList(l);System.out.println("""res0: List[Int] = """ + $show(res$0));$skip(17); val res$1 = 
  squareList2(l);System.out.println("""res1: List[Int] = """ + $show(res$1));$skip(56); 
  
  val test = List("a", "a", "a", "b", "c", "c", "a");System.out.println("""test  : List[java.lang.String] = """ + $show(test ));$skip(13); val res$2 = 
  pack(test);System.out.println("""res2: List[List[java.lang.String]] = """ + $show(res$2));$skip(15); val res$3 = 
  encode(test);System.out.println("""res3: List[(java.lang.String, Int)] = """ + $show(res$3));$skip(97); 
  
  def mapFun[T, U](xs: List[T], f: T => U): List[U] =
    (xs foldRight List[U]())(f(_) :: _);System.out.println("""mapFun: [T, U](xs: List[T], f: T => U)List[U]""");$skip(33); val res$4 = 

	mapFun(l, (x: Int) => (x * x));System.out.println("""res4: List[Int] = """ + $show(res$4));$skip(87); 
	
	def lengthFun[T](xs: List[T]): Int =
    (xs foldRight 0)((x: T, y: Int) => y + 1 );System.out.println("""lengthFun: [T](xs: List[T])Int""");$skip(18); val res$5 = 
  
  lengthFun(l);System.out.println("""res5: Int = """ + $show(res$5))}
}

  