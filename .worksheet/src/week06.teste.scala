package week06

object teste {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(62); 

	val xs = Array(1, 2, 3, 4, 5);System.out.println("""xs  : Array[Int] = """ + $show(xs ));$skip(19); val res$0 = 
	xs map (x => x*x);System.out.println("""res0: Array[Int] = """ + $show(res$0));$skip(23); 
	val s = "Hello World";System.out.println("""s  : java.lang.String = """ + $show(s ));$skip(31); val res$1 = 
	s filter (c => c.isUpperCase);System.out.println("""res1: String = """ + $show(res$1));$skip(33); val res$2 = 
	
	s exists (c => c.isUpperCase);System.out.println("""res2: Boolean = """ + $show(res$2));$skip(31); val res$3 = 
	s forall (c => c.isUpperCase);System.out.println("""res3: Boolean = """ + $show(res$3));$skip(35); 
	
	val pairs = List(1, 2, 3) zip s;System.out.println("""pairs  : List[(Int, Char)] = """ + $show(pairs ));$skip(13); val res$4 = 
	pairs.unzip;System.out.println("""res4: (List[Int], List[Char]) = """ + $show(res$4));$skip(54); val res$5 = 
	
	(1 to 2) flatMap (x => (1 to 3) map (y => (x, y)));System.out.println("""res5: scala.collection.immutable.IndexedSeq[(Int, Int)] = """ + $show(res$5));$skip(117); 
	
	def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double =
		(xs zip  ys).map{ case (x, y) => x * y}.sum;System.out.println("""scalarProduct: (xs: Vector[Double], ys: Vector[Double])Double""");$skip(103); 
		
	def isPrime(n: Int): Boolean =
	{
		val range = 2 until n
		range.forall((div) => n % div != 0)
	};System.out.println("""isPrime: (n: Int)Boolean""");$skip(110); 
	
	
	def isPrimeAndPrint(n: Int): Boolean =
	{
		if (isPrime(n))
		{
			println(""+ n)
			true
		}
		false
	};System.out.println("""isPrimeAndPrint: (n: Int)Boolean""");$skip(17); val res$6 = 
		
	
	isPrime(1);System.out.println("""res6: Boolean = """ + $show(res$6));$skip(12); val res$7 = 
	isPrime(2);System.out.println("""res7: Boolean = """ + $show(res$7));$skip(12); val res$8 = 
	isPrime(3);System.out.println("""res8: Boolean = """ + $show(res$8));$skip(12); val res$9 = 
	isPrime(4);System.out.println("""res9: Boolean = """ + $show(res$9));$skip(15); 
	
	
	val n = 5;System.out.println("""n  : Int = """ + $show(n ));$skip(77); val res$10 = 
	for {
		i <- 1 until n
		j <- 1 until i
		if isPrime(i + j)
	} yield (i, j);System.out.println("""res10: scala.collection.immutable.IndexedSeq[(Int, Int)] = """ + $show(res$10));$skip(114); 
	
	def scalarProductFor(xs: List[Double], ys: List[Double]): Double =
		(for((x, y) <- xs zip  ys) yield x*y).sum;System.out.println("""scalarProductFor: (xs: List[Double], ys: List[Double])Double""");$skip(44); val res$11 = 
	scalarProductFor(List(1,2,3), List(3,2,1));System.out.println("""res11: Double = """ + $show(res$11))}
}