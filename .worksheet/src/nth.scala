import week04._

object nth {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(186); 

	def nth[T](n: Int, list: List[T]): T =
		if (list.isEmpty) throw new IndexOutOfBoundsException
    else if(n == 0) list.head
    else nth(n -1, list.tail);System.out.println("""nth: [T](n: Int, list: week04.List[T])T""");$skip(64); 
   
  val list = new Cons(1, new Cons(2, new Cons(3, new Nil)));System.out.println("""list  : week04.Cons[Int] = """ + $show(list ));$skip(15); val res$0 = 
  nth(2, list);System.out.println("""res0: Int = """ + $show(res$0));$skip(15); val res$1 = 
  nth(4, list);System.out.println("""res1: Int = """ + $show(res$1))}
}