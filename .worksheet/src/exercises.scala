object exercises {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(62); 
  println("Welcome to the Scala worksheet");$skip(143); 
  
  def factorial(n: Int): Int =
  {
  	def loop(acc: Int, n: Int): Int =
  		if(n == 0) acc
  		else loop(n*acc, n -1)
  	
  	loop(1, n)
  };System.out.println("""factorial: (n: Int)Int""");$skip(191); 
  
  def sum(f: Int => Int)(a: Int, b: Int): Int =
  {
  	def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a + 1, acc + f(a))
        }
        loop(a, 0)
  };System.out.println("""sum: (f: Int => Int)(a: Int, b: Int)Int""");$skip(195); 
  
  def product(f: Int => Int)(a: Int, b: Int): Int =
  {
  	def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a + 1, acc * f(a))
        }
        loop(a, 1)
  };System.out.println("""product: (f: Int => Int)(a: Int, b: Int)Int""");$skip(60); 
  
  def factorialp(n: Int): Int =
  	product(x => x)(1, n);System.out.println("""factorialp: (n: Int)Int""");$skip(28); val res$0 = 
  	
  sum(x => x * x)(1, 4);System.out.println("""res0: Int = """ + $show(res$0));$skip(28); val res$1 = 
  product(x => x * x)(3, 4);System.out.println("""res1: Int = """ + $show(res$1))}
  
}