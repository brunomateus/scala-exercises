object mapReduce {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(62); 
  println("Welcome to the Scala worksheet");$skip(243); 
  
  def mapReduce(f:Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int) =
  {
    	def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a + 1, combine(acc, f(a)))
        }
        loop(a, zero)
  };System.out.println("""mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int)Int""");$skip(90); 
  
  def sum(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x + y, 0)(a, b);System.out.println("""sum: (f: Int => Int)(a: Int, b: Int)Int""");$skip(94); 
  
  def product(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x * y, 1)(a, b);System.out.println("""product: (f: Int => Int)(a: Int, b: Int)Int""");$skip(30); val res$0 = 
  
  
  sum(x => x * x)(1, 4);System.out.println("""res0: Int = """ + $show(res$0));$skip(28); val res$1 = 
  product(x => x * x)(3, 4);System.out.println("""res1: Int = """ + $show(res$1))}
}