import math.Ordering

object merge {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(80); 
  println("Welcome to the Scala worksheet");$skip(452); 
  def msort[T](list: List[T])(implicit ord: Ordering[T]): List[T] =
	{
		val index = list.length/2
		if (index == 0) list
		else
		{
			def merge(xs: List[T], ys: List[T]): List[T] =
			(xs, ys) match {
				case (Nil, ys) => ys
				case (xs, Nil) => xs
				case (x :: xs1, y :: ys1) =>
					if(ord.lt(x, y)) x :: merge(xs1, ys)
					else y :: merge(xs, ys1)
			}
			val (first, second) = list splitAt index
			merge(msort(first), msort(second))
		}
	};System.out.println("""msort: [T](list: List[T])(implicit ord: scala.math.Ordering[T])List[T]""");$skip(30); val res$0 = 
			
	msort(List(-2,4,1,4,-7));System.out.println("""res0: List[Int] = """ + $show(res$0))}
}