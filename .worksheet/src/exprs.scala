object exprs {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(113); 

	def show(e: Expr) = e match {
		case Number(n) => n
		case Sum(e1, e2) => show(e1) + show(e2)
	};System.out.println("""show: (e: <error>)<error>""")}
	
}