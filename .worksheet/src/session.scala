object session {import scala.runtime.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(60); 
  println("Welcome to the Scala worksheet");$skip(44); 
  def abs(x: Double) = if (x < 0) -x else x;System.out.println("""abs: (x: Double)Double""");$skip(130); 

  def sqrtIter(guess: Double, x: Double): Double =
    if (isGoodEnough(guess, x)) guess
    else sqrtIter(improve(guess, x), x);System.out.println("""sqrtIter: (guess: Double, x: Double)Double""");$skip(71); 

  def improve(guess: Double, x: Double) =
    (guess + x / guess) / 2;System.out.println("""improve: (guess: Double, x: Double)Double""");$skip(94); 
    
  def isGoodEnough(guess: Double, x: Double) =
    abs(guess * guess - x)/(2*x) <  0.001;System.out.println("""isGoodEnough: (guess: Double, x: Double)Boolean""");$skip(49); 

	def sqrt(x: Double): Double =
		sqrtIter(1, x);System.out.println("""sqrt: (x: Double)Double""");$skip(11); val res$0 = 
	
	sqrt(2);System.out.println("""res0: Double = """ + $show(res$0));$skip(9); val res$1 = 
	sqrt(4);System.out.println("""res1: Double = """ + $show(res$1));$skip(13); val res$2 = 
	sqrt(0.001);System.out.println("""res2: Double = """ + $show(res$2));$skip(15); val res$3 = 
	sqrt(0.1e-20);System.out.println("""res3: Double = """ + $show(res$3));$skip(14); val res$4 = 
	sqrt(1.0e20);System.out.println("""res4: Double = """ + $show(res$4));$skip(14); val res$5 = 
	sqrt(1.0e50);System.out.println("""res5: Double = """ + $show(res$5))}
	
	
	
}