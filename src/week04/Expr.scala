package week04

trait Expr {
  case class Number(n: Int) extends Expr
  case class Sum(e1: Expr, e2: Expr) extends Expr
}

object Number extends Expr
{
  def apply(n: Int) = new Number(n)
}

object Sum extends Expr
{
  def apply(e1: Expr, e2: Expr) = new Sum(e1, e2)
}