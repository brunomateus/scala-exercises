package week04

object exprs {

	def show(e: Expr): String = e match {
		case Number(n) => n.toString
		case Sum(l, r) => show(l) + " + " + show(r)
	}                                         //> show: (e: week04.Expr)String
	
	show(Sum(Number(3), Number(2)))           //> res0: String = 3 + 2
	
	
}