object list {
  def flatten(xs: List[Any]): List[Any] = xs match {
  	case List() => List()
  	case (y:List[Any]) :: ys => flatten(y ::: ys)
  	case y :: ys =>  y :: flatten(ys)
  	
  }                                               //> flatten: (xs: List[Any])List[Any]
  
 flatten(List(1,1))                               //> res0: List[Any] = List(1, 1)
 
 flatten(List(List(1,1)))                         //> res1: List[Any] = List(1, 1)
 
 flatten(List(List(1,1), 2))                      //> res2: List[Any] = List(1, 1, 2)
 
 flatten(List(List(List(1,1), 2)))                //> res3: List[Any] = List(1, 1, 2)

 flatten(List(List(1, 1), 2, List(3, List(5, 8))))//> res4: List[Any] = List(1, 1, 2, 3, 5, 8)
}