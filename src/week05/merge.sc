import math.Ordering

object merge {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  def msort[T](list: List[T])(implicit ord: Ordering[T]): List[T] =
	{
		val index = list.length/2
		if (index == 0) list
		else
		{
			def merge(xs: List[T], ys: List[T]): List[T] =
			(xs, ys) match {
				case (Nil, ys) => ys
				case (xs, Nil) => xs
				case (x :: xs1, y :: ys1) =>
					if(ord.lt(x, y)) x :: merge(xs1, ys)
					else y :: merge(xs, ys1)
			}
			val (first, second) = list splitAt index
			merge(msort(first), msort(second))
		}
	}                                         //> msort: [T](list: List[T])(implicit ord: scala.math.Ordering[T])List[T]
			
	msort(List(-2,4,1,4,-7))                  //> res0: List[Int] = List(-7, -2, 1, 4, 4)
}