package week06

object teste {

	val xs = Array(1, 2, 3, 4, 5)             //> xs  : Array[Int] = Array(1, 2, 3, 4, 5)
	xs map (x => x*x)                         //> res0: Array[Int] = Array(1, 4, 9, 16, 25)
	val s = "Hello World"                     //> s  : java.lang.String = Hello World
	s filter (c => c.isUpperCase)             //> res1: String = HW
	
	s exists (c => c.isUpperCase)             //> res2: Boolean = true
	s forall (c => c.isUpperCase)             //> res3: Boolean = false
	
	val pairs = List(1, 2, 3) zip s           //> pairs  : List[(Int, Char)] = List((1,H), (2,e), (3,l))
	pairs.unzip                               //> res4: (List[Int], List[Char]) = (List(1, 2, 3),List(H, e, l))
	
	(1 to 2) flatMap (x => (1 to 3) map (y => (x, y)))
                                                  //> res5: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((1,1), (1,2
                                                  //| ), (1,3), (2,1), (2,2), (2,3))
	
	def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double =
		(xs zip  ys).map{ case (x, y) => x * y}.sum
                                                  //> scalarProduct: (xs: Vector[Double], ys: Vector[Double])Double
		
	def isPrime(n: Int): Boolean =
	{
		val range = 2 until n
		range.forall((div) => n % div != 0)
	}                                         //> isPrime: (n: Int)Boolean
	
	
	def isPrimeAndPrint(n: Int): Boolean =
	{
		if (isPrime(n))
		{
			println(""+ n)
			true
		}
		false
	}                                         //> isPrimeAndPrint: (n: Int)Boolean
		
	
	isPrime(1)                                //> res6: Boolean = true
	isPrime(2)                                //> res7: Boolean = true
	isPrime(3)                                //> res8: Boolean = true
	isPrime(4)                                //> res9: Boolean = false
	
	
	val n = 5                                 //> n  : Int = 5
	for {
		i <- 1 until n
		j <- 1 until i
		if isPrime(i + j)
	} yield (i, j)                            //> res10: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((2,1), (3,
                                                  //| 2), (4,1), (4,3))
	
	def scalarProductFor(xs: List[Double], ys: List[Double]): Double =
		(for((x, y) <- xs zip  ys) yield x*y).sum
                                                  //> scalarProductFor: (xs: List[Double], ys: List[Double])Double
	scalarProductFor(List(1,2,3), List(3,2,1))//> res11: Double = 10.0
}