object mapReduce {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def mapReduce(f:Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int) =
  {
    	def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a + 1, combine(acc, f(a)))
        }
        loop(a, zero)
  }                                               //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b:
                                                  //|  Int)Int
  
  def sum(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x + y, 0)(a, b)
                                                  //> sum: (f: Int => Int)(a: Int, b: Int)Int
  
  def product(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x, y) => x * y, 1)(a, b)
                                                  //> product: (f: Int => Int)(a: Int, b: Int)Int
  
  
  sum(x => x * x)(1, 4)                           //> res0: Int = 30
  product(x => x * x)(3, 4)                       //> res1: Int = 144
}