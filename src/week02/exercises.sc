object exercises {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  def factorial(n: Int): Int =
  {
  	def loop(acc: Int, n: Int): Int =
  		if(n == 0) acc
  		else loop(n*acc, n -1)
  	
  	loop(1, n)
  }                                               //> factorial: (n: Int)Int
  
  def sum(f: Int => Int)(a: Int, b: Int): Int =
  {
  	def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a + 1, acc + f(a))
        }
        loop(a, 0)
  }                                               //> sum: (f: Int => Int)(a: Int, b: Int)Int
  
  def product(f: Int => Int)(a: Int, b: Int): Int =
  {
  	def loop(a: Int, acc: Int): Int = {
          if (a > b) acc
          else loop(a + 1, acc * f(a))
        }
        loop(a, 1)
  }                                               //> product: (f: Int => Int)(a: Int, b: Int)Int
  
  def factorialp(n: Int): Int =
  	product(x => x)(1, n)                     //> factorialp: (n: Int)Int
  	
  sum(x => x * x)(1, 4)                           //> res0: Int = 30
  product(x => x * x)(3, 4)                       //> res1: Int = 144
  
}