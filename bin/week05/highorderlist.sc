object highorderlist {
  
	def squareList(xs: List[Int]): List[Int] = xs match {
    case Nil     => xs
    case y :: ys => y*y :: squareList(ys)
  }                                               //> squareList: (xs: List[Int])List[Int]

  def squareList2(xs: List[Int]): List[Int] =
    xs map (n => n*n)                             //> squareList2: (xs: List[Int])List[Int]

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil      => Nil
    case x :: xs1 =>
    	val (first, last) = xs span((n => x == n))
    	first :: pack(last)
  }                                               //> pack: [T](xs: List[T])List[List[T]]
  
  def encode[T](xs: List[T]): List[(T, Int)] =
  	pack(xs) map (x => (x.head, x.length))    //> encode: [T](xs: List[T])List[(T, Int)]
  	
   
      
  val l = List(1,2,3,4,5)                         //> l  : List[Int] = List(1, 2, 3, 4, 5)
  
  squareList(l)                                   //> res0: List[Int] = List(1, 4, 9, 16, 25)
  squareList2(l)                                  //> res1: List[Int] = List(1, 4, 9, 16, 25)
  
  val test = List("a", "a", "a", "b", "c", "c", "a")
                                                  //> test  : List[java.lang.String] = List(a, a, a, b, c, c, a)
  pack(test)                                      //> res2: List[List[java.lang.String]] = List(List(a, a, a), List(b), List(c, c)
                                                  //| , List(a))
  encode(test)                                    //> res3: List[(java.lang.String, Int)] = List((a,3), (b,1), (c,2), (a,1))
}

  