scala-exercises
===============

Scala Lession Exercises
================
* week 01
  * square root function iterative
* week 02
  * tail recursive:
    * factorial
    * sum
    * product
    * factorial using product
    * map reduce
    * fixedpoint  cala-exercises
* week 03
  * Rational 
* week 04
  * List Trait
    * nth function
* week 05
  * List Flatten
  * Merge Sort with implicit parameters
  * High order function
* week 06 
  * Array and String as Sequence, ScalarProduct, isPrime
  * For expressions
